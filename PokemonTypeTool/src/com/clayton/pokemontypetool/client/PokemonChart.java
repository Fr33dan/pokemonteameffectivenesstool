package com.clayton.pokemontypetool.client;

import com.clayton.pokemontypetool.client.PokemonDB.PokemonType;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PokemonChart extends Composite {
    Grid gridPanel;
	
	public PokemonChart(){
		PokemonDB.PokemonType[] values = PokemonDB.PokemonType.values();
		gridPanel = new Grid(values.length + 1, values.length + 1);
		gridPanel.setText(0, 0, "");
		for(int j = 0;j < values.length;j++){
			PokemonDB.PokemonType t1 = values[j];
			Image newImage = new Image();
			Image newImage2 = new Image();

			newImage.setUrl("images\\types\\" + t1.toString() + "IC.gif");
			newImage2.setUrl("images\\types\\" + t1.toString() + "IC.gif");
			
			gridPanel.setWidget(j+ 1, 0, newImage);
			gridPanel.setWidget(0, j + 1, newImage2);
			

			gridPanel.getCellFormatter().setStyleName(j + 1, 0, "pokemontool-PokemonTypeCell");
			gridPanel.getCellFormatter().setStyleName(0, j + 1, "pokemontool-PokemonTypeCell");
			gridPanel.setStyleName("pokemonChart");
			for(int i = 0;i < values.length;i++){
				PokemonDB.PokemonType t2 = values[i];
				Label newLabel = new Label();
				double value = PokemonDB.getTypeEffectivenessAgainst(t1,t2);
				String valueText = Double.toString(value);
				
				newLabel.setTitle(t1.toString() + " does " + valueText + "x damage to " + t2.toString());

				if(value > 1){
					gridPanel.getCellFormatter().addStyleName(j + 1, i + 1,"superEffective");
					newLabel.setStyleName("superEffective");
				} else if(value == 0){
					gridPanel.getCellFormatter().addStyleName(j + 1, i + 1,"unEffective");
					newLabel.setStyleName("unEffective");
				}else if(value == .5){
					gridPanel.getCellFormatter().addStyleName(j + 1, i + 1,"notVeryEffective");
					newLabel.setStyleName("notVeryEffective");
					//valueText = "�";
				}
				
				newLabel.setText(valueText + "x");
				

				gridPanel.getCellFormatter().addStyleName(j + 1, i + 1,"pokemontool-PokemonTypeCell");
				gridPanel.setWidget(j + 1, i + 1, newLabel);
			}
		}
	    gridPanel.setStyleName("pokemontool-PokemonTypeChart");
		initWidget(gridPanel);
		
	}

}
