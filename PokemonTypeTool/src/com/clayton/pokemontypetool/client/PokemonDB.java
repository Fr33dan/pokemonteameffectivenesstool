package com.clayton.pokemontypetool.client;
import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Dictionary;

public class PokemonDB {
	public enum PokemonType {
		Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel, Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Dark, Fairy
	}
	
	public static double getTypeEffectivenessAgainst(PokemonType t1,PokemonType t2){
		switch(t1){
		case Normal:
			switch(t2){
			case Rock:
			case Steel:
				return .5;
			case Ghost:
				return 0;
			default:
				return 1;
			}
		case Fire:
			switch(t2){
			case Bug:
			case Steel:
			case Grass:
			case Ice:
				return 2;
			case Rock:
			case Fire:
			case Water:
			case Dragon:
				return .5;
			default:
				return 1;
			}
		case Fighting:
			switch(t2){
			case Normal:
			case Rock:
			case Steel:
			case Ice:
			case Dark:
				return 2;
			case Flying:
			case Poison:
			case Bug:
			case Psychic:
			case Fairy:
				return .5;
			case Ghost:
				return 0;
			default:
				return 1;
			}
		case Water:
			switch(t2){
			case Ground:
			case Rock:
			case Fire:
				return 2;
			case Water:
			case Grass:
			case Dragon:
				return .5;
			default:
				return 1;
			}
		case Flying:
			switch(t2){
			case Fighting:
			case Bug:
			case Grass:
				return 2;
			case Rock:
			case Steel:
			case Electric:
				return .5;
			default:
				return 1;
			}
			
		case Grass:
			switch(t2){
			case Ground:
			case Rock:
			case Water:
				return 2;
			case Flying:
			case Poison:
			case Bug:
			case Steel:
			case Fire:
			case Grass:
			case Dragon:
				return .5;
			default:
				return 1;
			}
		case Poison:
			switch(t2){
			case Grass:
			case Fairy:
				return 2;
			case Poison:
			case Rock:
			case Ground:
			case Ghost:
				return .5;
			case Steel:
				return 0;
			default:
				return 1;
			}
		case Electric:
			switch(t2){
			case Flying:
			case Fire:
				return 2;
			case Grass:
			case Electric:
			case Dragon:
				return .5;
			case Ground:
				return 0;
			default:
				return 1;
			}

		case Ground:
			switch(t2){
			case Poison:
			case Rock:
			case Steel:
			case Fire:
			case Electric:
				return 2;
			case Bug:
			case Grass:
				return .5;
			case Flying:
				return 0;
			default:
				return 1;
			}

		case Psychic:
			switch(t2){
			case Fighting:
			case Poison:
				return 2;
			case Steel:
			case Psychic:
				return .5;
			case Dark:
				return 0;
			default:
				return 1;
			}
		case Rock:
			switch(t2){
			case Flying:
			case Bug:
			case Fire:
			case Ice:
				return 2;
			case Fighting:
			case Ground:
			case Steel:
				return .5;
			default:
				return 1;
			}
		case Ice:
			switch(t2){
			case Flying:
			case Ground:
			case Grass:
			case Dragon:
				return 2;
			case Steel:
			case Fire:
			case Water:
			case Ice:
				return .5;
			default:
				return 1;
			}
		case Bug:
			switch(t2){
			case Grass:
			case Psychic:
			case Dark:
				return 2;
			case Fighting:
			case Flying:
			case Poison:
			case Ghost:
			case Steel:
			case Fire:
			case Fairy:
				return .5;
			default:
				return 1;
			}
		case Dragon:
			switch(t2){
			case Dragon:
				return 2;
			case Steel:
				return .5;
			case Fairy:
				return 0;
			default:
				return 1;
			}
		case Ghost:
			switch(t2){
			case Ghost:
			case Psychic:
				return 2;
			case Dark:
				return .5;
			case Normal:
				return 0;
			default:
				return 1;
			}

		case Dark:
			switch(t2){
			case Ghost:
			case Psychic:
				return 2;
			case Fighting:
			case Dark:
			case Fairy:
				return .5;
			default:
				return 1;
			}
		case Steel:
			switch(t2){
			case Rock:
			case Ice:
			case Fairy:
				return 2;
			case Steel:
			case Fire:
			case Water:
			case Electric:
				return .5;
			default:
				return 1;
			}
		case Fairy:
			switch(t2){
			case Fighting:
			case Dragon:
			case Dark:
				return 2;
			case Poison:
			case Steel:
			case Fire:
				return .5;
			default:
				return 1;
			}
		}
		return 1;
	}

	public static class PokemonInfo {
		public String name;
		public String ID;
		public int typeCount;
		public PokemonType type1;
		public PokemonType type2;
		
		public double getEffectivenessAgainst(PokemonInfo other){
			double returnVal;
			
			returnVal = getTypeEffectivenessAgainst(type1, other.type1);
			if(other.typeCount == 2){
				returnVal *= getTypeEffectivenessAgainst(type1, other.type2);
			}
			
			if(this.typeCount == 2){
				double newVal = getTypeEffectivenessAgainst(type2, other.type1);
				if(other.typeCount == 2){
					newVal *= getTypeEffectivenessAgainst(type2, other.type2);
				}
				
				if(newVal > returnVal){
					returnVal = newVal;
				}
					
			}
			return returnVal;
		}
		
		public String getImageURL(){
			return "images\\pokemon\\" + ID + "MS.png";
		}
		
		public String getType1ImageURL(){
			return "images\\types\\" + type1.toString() + "IC.gif";
		}
		
		public String getType2ImageURL(){
			return "images\\types\\" + type2.toString() + "IC.gif";
		}
	}

	private static Resources source = GWT.create(Resources.class);
	
	private static ArrayList<PokemonInfo> database;
	
	public static ArrayList<PokemonInfo> getDatabase(){
		if(database == null)
			load();
		return (ArrayList<PokemonInfo>)database.clone();
	}
	
	private static void load(){
		database = new ArrayList<PokemonInfo>();
		
		String db = source.pokemonInfo().getText();
		String[] lines = db.split("\\r?\\n");
		
		for(String line : lines){
			String[] info = line.split("[,]");
			PokemonInfo newInfo = new PokemonInfo();
			newInfo.ID = info[0];
			newInfo.name = info[1];
			newInfo.typeCount = Integer.parseInt(info[2]);
			newInfo.type1 = PokemonType.valueOf(info[3].trim());
			if(newInfo.typeCount == 2)
				newInfo.type2 = PokemonType.valueOf(info[4].trim());
			
			database.add(newInfo);
		}
	}

	static PokemonInfo getInfo(String numID) {
		if(database == null)
			load();
		
		for(PokemonInfo i:database){
			if(i.ID.equals(numID))
				return i;
		}
		return null;
	}

}
