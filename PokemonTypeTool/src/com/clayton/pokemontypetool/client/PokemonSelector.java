package com.clayton.pokemontypetool.client;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.clayton.pokemontypetool.client.PokemonDB.PokemonInfo;
import com.clayton.pokemontypetool.client.SelectorPopup.PokemonSelectedHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PokemonSelector extends Composite implements ClickHandler, PokemonSelectedHandler {
	public static SelectorPopup popup;
	public interface PokemonChangedHandler{
		public void pokemonChanged(PokemonInfo newPokemon);
	}
	public ArrayList<PokemonChangedHandler> changeHandlers;
    VerticalPanel rootPanel;
    FlowPanel typePanel;
    Label name;
    Image image;
    Image type1,type2;
    PokemonInfo currentInfo;
    
    
    
	
	
	public PokemonSelector(String numID){
		 // Place the check above the text box using a vertical panel.
	      this.rootPanel = new VerticalPanel();
	      this.typePanel = new FlowPanel();
	      this.image = new Image();
	      this.name = new Label();
	      this.type1 = new Image();
	      this.type2 = new Image();

	      rootPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	      
	      image.addClickHandler(this);
	      name.addClickHandler(this);
	      
	      name.setStyleName("pokemontool-SelectorText");
	      
	      typePanel.add(type1);
	      typePanel.add(type2);
	      
	      rootPanel.add(image);
	      rootPanel.add(typePanel);
	      rootPanel.add(name);

	      // All composites must call initWidget() in their constructors.
	      initWidget(rootPanel);

	      // Give the overall composite a style name.
	      setStyleName("pokemontool-PokemonSelector");
	      setNumber(numID);
	      
	}
	
	public void addPokemonChangeHandler(PokemonChangedHandler h){
		if(this.changeHandlers == null)
			this.changeHandlers = new ArrayList<PokemonChangedHandler>();
		
		if(!this.changeHandlers.contains(h))
			this.changeHandlers.add(h);
	}
	
	public void removePokemonChangeHandler(PokemonChangedHandler h){
		if(this.changeHandlers != null){
			changeHandlers.remove(h);
		}
	}
	
	public PokemonInfo getPokemonInfo(){
		return this.currentInfo;
	}
	public void setNumber(String ID){
		this.setInfo(PokemonDB.getInfo(ID));
	}
	
	public void setInfo(PokemonInfo newInfo){ 
		currentInfo = newInfo;
		this.image.setUrl(currentInfo.getImageURL());
		this.name.setText(currentInfo.name);
		this.type1.setUrl(currentInfo.getType1ImageURL());
		if(currentInfo.typeCount == 2){
			this.type2.setUrl(currentInfo.getType2ImageURL());
			this.type2.setVisible(true);
		} else{
			this.type2.setVisible(false);
		}
		
	}

	@Override
	public void onClick(ClickEvent event) {
		if(popup == null){
			popup = new SelectorPopup();
		}
		popup.setPokemonSelectedHandler(this);
		popup.showRelativeTo(this);
	}

	@Override
	public void pokemonSelected(PokemonInfo newPokemon) {
		this.setInfo(newPokemon);
		
		if(this.changeHandlers != null){
			for(PokemonChangedHandler h:this.changeHandlers){
				h.pokemonChanged(newPokemon);
			}
		}
		
	}

}
