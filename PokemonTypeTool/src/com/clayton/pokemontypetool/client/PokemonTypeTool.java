package com.clayton.pokemontypetool.client;

import java.util.ArrayList;

import com.clayton.pokemontypetool.client.PokemonDB.PokemonInfo;
import com.clayton.pokemontypetool.client.PokemonSelector.PokemonChangedHandler;
import com.clayton.pokemontypetool.shared.FieldVerifier;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class PokemonTypeTool implements EntryPoint, ClickHandler {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	private static class MessagePopup extends PopupPanel {
		Label l;
		public MessagePopup() {
			// PopupPanel's constructor takes 'auto-hide' as its boolean
			// parameter.
			// If this is set, the panel closes itself automatically when the
			// user
			// clicks outside of it.
			super(true);
			l = new Label("Click outside of this popup to close it");

			// PopupPanel is a SimplePanel, so you have to set it's widget
			// property to
			// whatever you want its contents to be.
			setWidget(l);
		}
		
		public void setText(String text){
			l.setText(text);
		}
		
		
	}

	/**
	 * Create a remote service proxy to talk to the server-side Greeting
	 * service.
	 */
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	private FlowPanel output;
	private Image[] outputPokemon;
	private Label summaryLabel;
	private MessagePopup popup;

	private com.clayton.pokemontypetool.client.PokemonSelector[] selectors;

	public void calculateOutput() {
		ArrayList<PokemonInfo> database = PokemonDB.getDatabase();

		if (outputPokemon == null) {
			outputPokemon = new Image[database.size()];
		}

		// output.clear();

		if (summaryLabel == null) {
			summaryLabel = new Label();
			output.add(summaryLabel);
		}
		int supereffectiveCount = 0;
		for (int j = 0; j < database.size(); j++) {
			PokemonInfo opponent = database.get(j);
			double maxEffectiveness = 0;
			// HorizontalPanel matchupPanel = new HorizontalPanel();

			if (outputPokemon[j] == null) {
				outputPokemon[j] = new Image();
				outputPokemon[j].setUrl(opponent.getImageURL());
				outputPokemon[j].setAltText(opponent.name);
				outputPokemon[j].addClickHandler(this);
				output.add(outputPokemon[j]);
			}

			// matchupPanel.add(newImage);

			PokemonInfo bestMatch = null;
			for (PokemonSelector s : selectors) {
				PokemonInfo teamMember = s.getPokemonInfo();
				double effectiveness = teamMember.getEffectivenessAgainst(opponent);

				if (effectiveness > maxEffectiveness) {
					maxEffectiveness = effectiveness;
					bestMatch = teamMember;
				}
			}

			outputPokemon[j].setTitle(
					opponent.name + " is done " + Double.toString(maxEffectiveness) + "x damage by " + bestMatch.name);

			outputPokemon[j].removeStyleName("ultraEffective");
			outputPokemon[j].removeStyleName("superEffective");
			outputPokemon[j].removeStyleName("notVeryEffective");
			outputPokemon[j].removeStyleName("evenLessEffective");
			outputPokemon[j].removeStyleName("unEffective");

			if (maxEffectiveness == 4.0) {
				supereffectiveCount++;
				outputPokemon[j].addStyleName("ultraEffective");
			} else if (maxEffectiveness == 2.0) {
				supereffectiveCount++;
				outputPokemon[j].addStyleName("superEffective");
			} else if (maxEffectiveness == 0.5) {
				outputPokemon[j].addStyleName("notVeryEffective");
			} else if (maxEffectiveness == 0.25) {
				outputPokemon[j].addStyleName("evenLessEffective");
			} else if (maxEffectiveness == 0) {
				outputPokemon[j].addStyleName("unEffective");
				outputPokemon[j].setTitle(opponent.name + " is completely unaffected by all pokemon on this team.");
			}
		}
		double supereffectiveRatio = (1.0 * supereffectiveCount) / database.size();
		summaryLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		summaryLabel.setText("This team is supereffective against "
				+ NumberFormat.getFormat("#.00").format(supereffectiveRatio * 100) + "% of pokemon:");
	}

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final PokemonChart chart = new PokemonChart();
		selectors = new PokemonSelector[6];

		for (int j = 0; j < selectors.length; j++) {
			String num = Integer.toString(j + 1);
			selectors[j] = new PokemonSelector("00" + num);
			selectors[j].addPokemonChangeHandler(new PokemonChangedHandler() {
				public void pokemonChanged(PokemonInfo i) {
					calculateOutput();

				}
			});
			RootPanel.get("pokemonSelector" + num).add(selectors[j]);
		}
		output = new FlowPanel();

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("pokemonChart").add(chart);
		RootPanel.get("outputContainer").add(output);
		calculateOutput();
	}

	@Override
	public void onClick(ClickEvent event) {
		Image pokemon = (Image) event.getSource();
		if(popup==null){
			popup = new MessagePopup();
		}
		popup.setText(pokemon.getTitle());
		popup.showRelativeTo(pokemon);
	}
}
