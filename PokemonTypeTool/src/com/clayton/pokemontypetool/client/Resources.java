package com.clayton.pokemontypetool.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface Resources extends ClientBundle {

	  @Source("PokemonInfo.txt")
	  TextResource pokemonInfo();
}
