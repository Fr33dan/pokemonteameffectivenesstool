package com.clayton.pokemontypetool.client;

import java.util.ArrayList;

import com.clayton.pokemontypetool.client.PokemonDB.PokemonInfo;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SelectorPopup extends PopupPanel implements ClickHandler, KeyUpHandler {
	public interface PokemonSelectedHandler{
		public void pokemonSelected(PokemonInfo newPokemon);
	}
	ArrayList<PokemonInfo> database = PokemonDB.getDatabase();
    VerticalPanel rootPanel;
    ExtendedTextBox inputBox;
    ScrollPanel scrollPanel;
    FlowPanel flowPanel;
    
    Image[] pokemonImages;
    
    private PokemonSelectedHandler handler;
	
	public SelectorPopup(){
		super(true);
		rootPanel = new VerticalPanel();
		inputBox = new ExtendedTextBox();
		flowPanel = new FlowPanel();
		scrollPanel = new ScrollPanel(flowPanel);
		
		inputBox.addKeyUpHandler(this);
		
		ArrayList<PokemonInfo> database = PokemonDB.getDatabase();
		pokemonImages = new Image[database.size()];
		for(int j = 0;j < pokemonImages.length;j++){
			pokemonImages[j] = new Image();
			pokemonImages[j].setUrl(database.get(j).getImageURL());
			pokemonImages[j].addClickHandler(this);
			pokemonImages[j].setAltText(database.get(j).name);
			pokemonImages[j].setTitle(database.get(j).name);
			flowPanel.add(pokemonImages[j]);
		}
		
		rootPanel.add(inputBox);
		rootPanel.add(scrollPanel);

		scrollPanel.setWidth((Window.getClientWidth()/4) + "px");
		scrollPanel.setHeight((Window.getClientWidth()/4) + "px");
		
		this.setWidget(rootPanel);
		
	}
	
	
	@Override
	public void show(){

		scrollPanel.setWidth((Window.getClientWidth()/4) + "px");
		scrollPanel.setHeight((Window.getClientWidth()/4) + "px");
		
		for(int j = 0;j < pokemonImages.length;j++){
			pokemonImages[j].setVisible(true);
		}
		inputBox.setText("");
		super.show();
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

		    @Override
		    public void execute() {
		    	inputBox.setFocus(true);
		    }
		});
	}
	
	public void setPokemonSelectedHandler(PokemonSelectedHandler h){
		this.handler = h;
	}
	

	@Override
	public void onClick(ClickEvent event) {
		
		if(handler != null){
			int index = 0;
			for(int j = 0;j < pokemonImages.length;j++){
				if(event.getSource() == pokemonImages[j])
					index = j;
			}
			
			handler.pokemonSelected(database.get(index));
		}
		this.hide();
	}

	@Override
	public void onKeyUp(KeyUpEvent event) {

    	for(int j = 0;j < pokemonImages.length;j++){
    		PokemonInfo i = database.get(j);
    		boolean isMatch = i.name.toLowerCase().contains(inputBox.getText().toLowerCase());
    		
    		isMatch |= inputBox.getText() == "";
			pokemonImages[j].setVisible(isMatch);
		}
		
	}

}
